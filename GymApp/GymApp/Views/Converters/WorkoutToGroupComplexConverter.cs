﻿using GymApp.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace GymApp.Views.Converters
{
    public class WorkoutToGroupComplexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Workout workout = value as Workout;

            switch (workout.Type)
            {
                case WorkoutType.Cardio:
                    return "Cardio";
                case WorkoutType.Strength_Training:
                    return "Strength Training";
                case WorkoutType.Other:
                    return "Other Type Of Exercise";
                default:
                    return "No Group";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
