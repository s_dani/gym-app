﻿using GymApp.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace GymApp.Views.Converters
{
    public class WorkoutTypeToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            WorkoutType type = (WorkoutType)Enum.Parse(typeof(WorkoutType), value.ToString());

            switch (type)
            {
                case WorkoutType.Cardio:
                    return new SolidColorBrush(Colors.Green);
                case WorkoutType.Strength_Training:
                    return new SolidColorBrush(Colors.Red);
                case WorkoutType.Other:
                    return new SolidColorBrush(Colors.Purple);
                default:
                    return new SolidColorBrush(Colors.Black);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
