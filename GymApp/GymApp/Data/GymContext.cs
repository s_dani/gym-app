﻿using GymApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Data
{
    public class GymContext : DbContext
    {
        public virtual DbSet<Workout> Workouts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        public GymContext()
        {
            this.Database.EnsureCreated();
        }

        public GymContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database.mdf;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            User Dani = new User() { Id = 1, UserName = "Dani", Age = 25, Height = 185, Weight = 75 };
            User Csaba = new User() { Id = 2, UserName = "Csabi", Age = 23, Height = 175, Weight = 68 };

            Workout Running = new Workout() { Id = 1, UserId = Dani.Id, Title = "Running", Date = DateTime.Now, Duration = 20, Type = WorkoutType.Cardio };
            Workout Swimming = new Workout() { Id = 2, UserId = Dani.Id, Title = "Swimming", Date = DateTime.Now, Duration = 10, Type = WorkoutType.Other };
            Workout Cycling = new Workout() { Id = 3, UserId = Csaba.Id, Title = "Cycling", Date = DateTime.Now, Duration = 80, Type = WorkoutType.Other };

            modelBuilder.Entity<User>().HasData(Dani, Csaba);
            modelBuilder.Entity<Workout>().HasData(Running, Swimming, Cycling);
            modelBuilder.Entity<User>().HasMany(u => u.Workouts).WithOne(w => w.User).HasForeignKey(w => w.UserId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
