﻿using GymApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Data.Logic
{
    public class WorkoutRepository : Repository<Workout>
    {
        public WorkoutRepository(GymContext dbContext) : base(dbContext)
        {
        }

        public override Workout GetOne(int id)
        {
            return GetAll().Where(workout => workout.Id == id).FirstOrDefault();
        }
    }
}
