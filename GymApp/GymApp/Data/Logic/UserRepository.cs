﻿using GymApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Data.Logic
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(GymContext dbContext) : base(dbContext)
        {
        }

        public override User GetOne(int id)
        {
            return GetAll().Where(user => user.Id == id).FirstOrDefault();
        }
    }
}
