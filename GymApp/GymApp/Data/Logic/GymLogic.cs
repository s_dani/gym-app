﻿using GymApp.Models;
using GymApp.ViewModels;
using GymApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GymApp.Data.Logic
{
    public class GymLogic
    {
        private UserRepository userRepository;
        private WorkoutRepository workoutRepository;

        public GymLogic(UserRepository userRepository, WorkoutRepository workoutRepository)
        {
            this.userRepository = userRepository;
            this.workoutRepository = workoutRepository;
        }

        public bool AddUser()
        {
            User newUser = new User();
            AddUserWindow window = new AddUserWindow();
            (window.DataContext as AddUserWindowVM).User = newUser;

            if (window.ShowDialog() == true)
            {
                userRepository.Insert(newUser);
                return true;
                //MessageBox.Show("User added!");
            }
            else
            {
                return false;
            }
        }

        public bool RemoveUser(User user)
        {
            if (user != null)
            {
                userRepository.Remove(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        public User FindUser(int id)
        {
            User foundUser = userRepository.GetOne(id);
            if (foundUser != null)
            {
                MessageBox.Show("User found!");
                return foundUser;
            }
            else
            {
                MessageBox.Show("User was not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public bool UpdateUser(User user)
        {
            if (user != null)
            {
                AddUserWindow window = new AddUserWindow();
                (window.DataContext as AddUserWindowVM).User = user;

                if (window.ShowDialog() == true)
                {
                    userRepository.Update(user);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                MessageBox.Show("User was not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public List<User> GetAllUsers()
        {
            return userRepository.GetAll();
        }

        public bool AddWorkout(User selectedUser)
        {
            Workout workout = new Workout();
            workout.UserId = selectedUser.Id;
            workout.Date = DateTime.Now;
            AddWorkoutWindow window = new AddWorkoutWindow();
            (window.DataContext as AddWorkoutWindowVM).Workout = workout;

            if (window.ShowDialog() == true)
            {
                workoutRepository.Insert(workout);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool RemoveWorkout(Workout workout)
        {
            if (workout != null)
            {
                workoutRepository.Remove(workout);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Workout FindWorkout(int id)
        {
            Workout foundWorkout = workoutRepository.GetOne(id);

            if (foundWorkout != null)
            {
                MessageBox.Show("Workout found!");
                return foundWorkout;
            }
            else
            {
                MessageBox.Show("Workout was not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }

        public bool UpdateWorkout(Workout workout)
        {
            if (workout != null)
            {
                AddWorkoutWindow window = new AddWorkoutWindow();
                (window.DataContext as AddWorkoutWindowVM).Workout = workout;

                if (window.ShowDialog() == true)
                {
                    workoutRepository.Update(workout);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Workout was not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        public List<Workout> GetAllWorkouts()
        {
            return workoutRepository.GetAll();
        }

        public void ShowWorkouts(User selectedUser)
        {
            if (selectedUser != null)
            {
                WorkoutsWindow window = new WorkoutsWindow();
                (window.DataContext as WorkoutsWindowVM).SelectedUser= selectedUser;
                (window.DataContext as WorkoutsWindowVM).GymLogic = this;
                (window.DataContext as WorkoutsWindowVM).FillWorkouts();
                window.Show();
            }
            else
            {
                MessageBox.Show("User was not found!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
