﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Data.Logic
{
    public abstract class Repository<T> where T : class
    {
        private GymContext dbContext;

        public Repository(GymContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void Insert(T item)
        {
            dbContext.Set<T>().Add(item);
            dbContext.SaveChanges();
        }

        public void Remove(T item)
        {
            dbContext.Set<T>().Remove(item);
            dbContext.SaveChanges();
        }

        public void Update(T item)
        {
            dbContext.Set<T>().Update(item);
            dbContext.SaveChanges();
        }

        public List<T> GetAll()
        {
            return dbContext.Set<T>().ToList();
        }

        public abstract T GetOne(int id);
    }
}
