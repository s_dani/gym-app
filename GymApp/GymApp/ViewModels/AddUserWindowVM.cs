﻿using GymApp.Models;
using GymApp.ViewModels.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.ViewModels
{
    public class AddUserWindowVM : ObservableObject
    {
        private User user;

        public User User
        {
            get { return user; }
            set
            {
                if (user == value)
                {
                    return;
                }
                user = value;
                OnPropertyChanged();
            }
        }
    }
}
