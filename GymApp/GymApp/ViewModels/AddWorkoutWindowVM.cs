﻿using GymApp.Models;
using GymApp.ViewModels.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.ViewModels
{
    public class AddWorkoutWindowVM : ObservableObject
    {
        private Workout workout;

        public Workout Workout
        {
            get { return workout; }
            set
            {
                if (workout == value)
                {
                    return;
                }
                workout = value;
                OnPropertyChanged();
            }
        }

        public Array WorkoutTypes
        {
            get
            {
                return Enum.GetValues(typeof(WorkoutType));
            }
        }
    }
}
