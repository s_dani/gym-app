﻿using GymApp.Data;
using GymApp.Data.Logic;
using GymApp.Models;
using GymApp.ViewModels.Implementations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GymApp.ViewModels
{
    public class MainWindowVM : ObservableObject
    {
        private User selectedUser;
        private ObservableCollection<User> users;
        private GymLogic gymLogic;
        private GymContext dbContext;
        private string searchText;

        public ICommand AddUserCommand { get; set; }
        public ICommand DeleteUserCommand { get; set; }
        public ICommand UpdateUserCommand { get; set; }
        public ICommand ShowWorkoutsCommand { get; set; }

        public User SelectedUser
        {
            get { return selectedUser; }
            set
            {
                if (selectedUser == value)
                {
                    return;
                }
                selectedUser = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<User> Users
        {
            get { return users; }
            set
            {
                if (users == value)
                {
                    return;
                }
                users = value;
                OnPropertyChanged();
            }
        }

        public string SearchText
        {
            get { return searchText; }
            set
            {
                if (searchText == value)
                {
                    return;
                }
                searchText = value;
                OnPropertyChanged();
                Search();
            }
        }

        public MainWindowVM()
        {
            this.Users = new ObservableCollection<User>();
            this.dbContext = new GymContext();
            UserRepository userRepository = new UserRepository(dbContext);
            WorkoutRepository workoutRepository = new WorkoutRepository(dbContext);
            this.gymLogic = new GymLogic(userRepository, workoutRepository);
            this.AddUserCommand = new RelayCommand((obj) => AddUser());
            this.DeleteUserCommand = new RelayCommand((obj) => RemoveUser());
            this.UpdateUserCommand = new RelayCommand((obj) => UpdateUser());
            this.ShowWorkoutsCommand = new RelayCommand((obj) => ShowWorkouts());
            FillUsers();
        }

        private void FillFilteredUsers(List<User> filteredUsers)
        {
            Users.Clear();
            filteredUsers.ForEach(fUser => Users.Add(fUser));
        }

        private void FillUsers()
        {
            Users.Clear();
            gymLogic.GetAllUsers().ForEach(user => Users.Add(user));
        }

        private void AddUser()
        {
            if (gymLogic.AddUser())
            {
                FillUsers();
                MessageBox.Show("User added!");
            }
            else
            {
                MessageBox.Show("User was not added!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RemoveUser()
        {
            if (gymLogic.RemoveUser(SelectedUser))
            {
                FillUsers();
                MessageBox.Show("User removed!");
            }
            else
            {
                MessageBox.Show("User was not removed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateUser()
        {
            if (gymLogic.UpdateUser(SelectedUser))
            {
                FillUsers();
                MessageBox.Show("User updated!");
            }
            else
            {
                MessageBox.Show("User was not updated!");
            }
        }

        private void ShowWorkouts()
        {
            gymLogic.ShowWorkouts(SelectedUser);
        }

        private void Search()
        {
            var filteredUsers = gymLogic.GetAllUsers().Where(user => user.UserName.ToLower().Contains(searchText.ToLower())).ToList();
            FillFilteredUsers(filteredUsers);
        }
    }
}
