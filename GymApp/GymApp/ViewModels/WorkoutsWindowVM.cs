﻿using GymApp.Data.Logic;
using GymApp.Models;
using GymApp.ViewModels.Implementations;
using GymApp.Views.Converters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace GymApp.ViewModels
{
    public class WorkoutsWindowVM : ObservableObject
    {
        private Workout selectedWorkout;
        private ObservableCollection<Workout> workouts;
        public GymLogic GymLogic { get; set; }
        public User SelectedUser { get; set; }
        
        private Brush fontColor;
        private Style mainStyle;

        public Workout SelectedWorkout
        {
            get { return selectedWorkout; }
            set
            {
                if (selectedWorkout == value)
                {
                    return;
                }
                selectedWorkout = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Workout> Workouts
        {
            get { return workouts; }
            set 
            {
                if (workouts == value)
                {
                    return;
                }
                workouts = value;
                OnPropertyChanged();
            }
        }

        public Brush FontColor
        {
            get { return fontColor; }
            set
            {
                if (fontColor == value)
                {
                    return;
                }
                fontColor = value;
                OnPropertyChanged();
            }
        }

        public Style MainStyle
        {
            get { return mainStyle; }
            set
            {
                if (mainStyle == value)
                {
                    return;
                }
                mainStyle = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddWorkoutCommand { get; set; }
        public ICommand DeleteWorkoutCommand { get; set; }
        public ICommand UpdateWorkoutCommand { get; set; }
        public ICommand GroupWorkoutsCommand { get; set; }
        public ICommand ChangeThemeCommand { get; set; }

        private ICollectionView WorkoutsCollectionView
        {
            get { return CollectionViewSource.GetDefaultView(Workouts); }
        }

        public WorkoutsWindowVM()
        {
            this.MainStyle = (Style)Application.Current.FindResource("GridMainStyle");
            this.FontColor = Brushes.Black;
            this.Workouts = new ObservableCollection<Workout>();
            this.AddWorkoutCommand = new RelayCommand((obj) => AddWorkout());
            this.DeleteWorkoutCommand = new RelayCommand((obj) => RemoveWorkout());
            this.UpdateWorkoutCommand = new RelayCommand((obj) => UpdateWorkout());
            this.GroupWorkoutsCommand = new RelayCommand((obj) => GroupWorkouts());
            this.ChangeThemeCommand = new RelayCommand((obj) => ChangeTheme());
        }

        private List<Workout> GetSelectedUsersWorkouts()
        {
            return GymLogic.GetAllWorkouts().Where(workout => workout.UserId == SelectedUser.Id).ToList();
        }

        public void FillWorkouts()
        {
            Workouts.Clear();
            GetSelectedUsersWorkouts().ForEach(workout => Workouts.Add(workout));
        }

        private void AddWorkout()
        {
            if (GymLogic.AddWorkout(SelectedUser))
            {
                FillWorkouts();
                MessageBox.Show("Workout added!");
            }
            else
            {
                MessageBox.Show("Workout was not added!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RemoveWorkout()
        {
            if (GymLogic.RemoveWorkout(SelectedWorkout))
            {
                FillWorkouts();
                MessageBox.Show("Workout removed!");
            }
            else
            {
                MessageBox.Show("Workout was not removed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateWorkout()
        {
            if (GymLogic.UpdateWorkout(SelectedWorkout))
            {
                FillWorkouts();
                MessageBox.Show("Workout updated!");
            }
            else
            {
                MessageBox.Show("Workout was not updated!");
            }
        }

        private void GroupWorkouts()
        {
            var groupDescriptions = WorkoutsCollectionView.GroupDescriptions;

            if (groupDescriptions.Count == 0)
            {
                //groupDescriptions.Add(new PropertyGroupDescription("Type"));
                groupDescriptions.Add(new PropertyGroupDescription(null, new WorkoutToGroupComplexConverter()));
            }
            else
            {
                groupDescriptions.Clear();
            }
        }

        private void ChangeTheme()
        {
            var currentApp = Application.Current;

            Style GridMainStyle = (Style)currentApp.FindResource("GridMainStyle");
            Style GridDarkStyle = (Style)currentApp.FindResource("GridDarkStyle");

            if (MainStyle == GridMainStyle)
            {
                MainStyle = GridDarkStyle;
                FontColor = Brushes.GhostWhite;
            }
            else if (MainStyle == GridDarkStyle)
            {
                MainStyle = GridMainStyle;
                FontColor = Brushes.Black;
            }
        }
    }
}
