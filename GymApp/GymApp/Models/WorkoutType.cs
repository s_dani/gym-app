﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Models
{
    public enum WorkoutType
    {
        Cardio,
        Strength_Training,
        Other
    }
}
