﻿using GymApp.ViewModels.Implementations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Models
{
    public class Workout
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        public WorkoutType Type { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public double Duration { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }

        [NotMapped]
        public virtual User User { get; set; }
    }
}
