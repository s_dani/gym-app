﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymApp.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string UserName { get; set; }

        [Required]
        public int Age { get; set; }

        [Required]
        public double Weight { get; set; }

        [Required]
        public int Height { get; set; }

        [NotMapped]
        public virtual ICollection<Workout> Workouts { get; set; }

        public User()
        {
            this.Workouts = new HashSet<Workout>();
        }
    }
}
